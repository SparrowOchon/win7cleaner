set-executionpolicy remotesigned
Add-Type -AssemblyName System.IO.Compression.FileSystem

#Setup PSWindowsUpdate Module (Needed for Update Management)------------------------------------------------------------------------------------------------------------------------------------------
$powershell_module= "https://gallery.technet.microsoft.com/scriptcenter/2d191bcd-3308-4edd-9de2-88dff796b0bc/file/41459/47/PSWindowsUpdate.zip"
$module_install_dir = $env:windir + "\System32\WindowsPowerShell\v1.0\Modules"
$path = $module_install_dir + "\PSWindowsUpdate"
$temp_folder = [System.IO.Path]::GetTempFileName();
remove-item $temp_folder;
new-item -type directory -path $temp_folder;

If(!(test-path $path)){
	$temp_zip = $temp_folder + "\PSWindowsUpdate.zip"
		$client = new-object System.Net.WebClient
		$client.DownloadFile($powershell_module, $temp_zip)
		[System.IO.Compression.ZipFile]::ExtractToDirectory($temp_zip, $module_install_dir)
}
Import-Module PSWindowsUpdate
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


#Start removing updates(Takes a while) --------------------------------------------------------------------------------------------------------------------------------------------------------------
$remove_updates = {
	$MAX_THREADS = 15
	$updates_list=('KB2505438', 'KB2670838', 'KB2882822', 'KB2952664',
			'KB2976978', 'KB2976987', 'KB2977759', 'KB2990214', 'KB3012973',
			'KB3015249', 'KB3021917', 'KB3022345', 'KB3035583', 'KB3042058',
			'KB3044374', 'KB3050267', 'KB3064683', 'KB3065987', 'KB3065988',
			'KB3068707', 'KB971033', 'KB454826', 'KB3150513', 'KB3148198','KB3068708',
			'KB3146449', 'KB3138612', 'KB3135449', 'KB3135445', 'KB3123862','KB3074677',
			'KB3118401', 'KB3112343', 'KB3112336', 'KB3107998', 'KB3093983','KB3072318',
			'KB3102810', 'KB3102812', 'KB3090045', 'KB3088195', 'KB3086255','KB3068708',
			'KB3083711', 'KB3083710', 'KB3083325', 'KB3083324', 'KB3081954','KB3022345',
			'KB3081454', 'KB3081437', 'KB3080149', 'KB3075853','KB3075851', 'KB3075249',
			'KB3075249', 'KB3080149', 'KB3112343', 'KB3135445', 'KB3021917', 'KB3035583',
			'KB2990214', 'KB2952664', 'KB3065987', 'KB3050265', 'KB2976987', 'KB2902907',
			'KB3102810', 'KB3123862', 'KB3081954', 'KB3139929', 'KB3138612', 'KB3150513',
			'KB3133977', 'KB3139923', 'KB3173040', 'KB3146449', 'KB3044374', 'KB2976978',
			'KB3012973', 'KB2977759', 'KB971033', 'KB3065988', 'KB3083325', 'KB3083324',
			'KB3075853','KB3050267', 'KB3075851', 'KB3046480', 'KB3083710', 'KB3083711',
			'KB3112336','KB3103696','KB3121260', 'KB3099834', 'KB3084905', 'KB3029606',
			'KB3134815', 'KB3139921','KB3126030', 'KB3060746','KB3102429', 'KB3092627',
			'KB3080800', 'KB3078405', 'KB3080042', 'KB3103699','KB3100956', 'KB3054464',
			'KB3061493', 'KB3133924', 'KB3060793', 'KB3063843','KB3072019', 'KB3126033',
			'KB3095701', 'KB3056347','KB3100919', 'KB3123242', 'KB3079318', 'KB3018467',
			'KB3013791', 'KB3078676','KB3059316', 'KB3045746','KB3128650', 'KB3055343',
			'KB3118401', 'KB3071663','KB3091297', 'KB3096433','KB3045634', 'KB3065013',
			'KB3125210', 'KB3029603', 'KB3029432', 'KB3053863','KB3112148', 'KB3064059',
			'KB3060383', 'KB3087041','KB3121255', 'KB3054256', 'KB3107998', 'KB3121261',
			'KB3041857', 'KB3087137','KB3132080', 'KB3130896','KB3058168', 'KB3055323')

	Foreach($update in $updates_list){
		While($(Get-Job -State Running).Count -ge $MAX_THREADS) {
			Get-Job | Wait-Job -Any | Out-Null
		}
		Start-Job -ArgumentList $update -Name "Remove:$update" {
			param($update)
				$item = $update.Substring(2)
				wusa /uninstall /kb:$item /quiet /norestart | Out-Null
		}
	}
	Get-Job | Wait-Job
	Hide-WUUpdate -KBArticleID $updates_list -HideStatus:$true -Confirm:$false
}
Start-Job -Name RemoveUpdates -scriptblock $remove_updates
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


# Disable Windows 7 Microsoft Features-----------------------------------------------------------------------------------------------------------------------------------------------------------------
$disable_features = {
	$MAX_THREADS = 20
	Start-Process -Wait -FilePath "rundll32.exe" -ArgumentList "$env:SystemRoot\system32\shell32.dll,Control_RunDLL $env:SystemRoot\system32\desk.cpl desk,@Themes /Action:OpenTheme /file:""C:\Windows\Resources\Ease of Access Themes\classic.theme"""

	$features_list=('WindowsGadgetPlatform','InboxGames','More Games','Solitaire','SpiderSolitaire','Hearts','FreeCell','Minesweeper',
			'PurblePlace','Chess','Shanghai','Internet Games','Internet Checkers','Internet Backgammon','Internet Spades','MediaPlayback','WindowsMediaPlayer','MediaCenter','OpticalMediaDisc','NetFx3','TabletPCOC','Printing-Foundation-Features','Printing-Foundation-LPRPortMonitor','Printing-Foundation-InternetPrinting-Client','FaxServicesClientPackage','Printing-XPSServices-Features','Xps-Foundation-Xps-Viewer','Internet-Explorer-Optional-amd64')

	Foreach ($item in $features_list){
		While($(Get-Job -State Running).Count -ge $MAX_THREADS) {
			Get-Job | Wait-Job -Any | Out-Null
		}
		Start-Job -ArgumentList $item -Name "Disable:$item" {
			dism /online /Disable-Feature /FeatureName:$item /quiet /norestart | Out-Null
		}
	}
	Get-Job | Wait-Job
}
Start-Job -Name DisableFeatures -scriptblock $disable_features
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


# Disable Services According to BlackVipers Tweaked----------------------------------------------------------------------------------------------------------------------------------------------------
$disable_services = {
	$service_list = @{ AxInstSV = 'Disabled';  SensrSvc = 'Disabled';  AeLookupSvc = 'Manual';  AppIDSvc = 'Manual';  Appinfo = 'Manual';  ALG = 'Disabled';  AppMgmt = 'Disabled';  BITS = 'Manual';  BFE = 'Automatic';  BDESVC = 'Disabled';  wbengine = 'Manual';  bthserv = 'Disabled';  PeerDistSvc = 'Disabled';  CertPropSvc = 'Disabled';  KeyIso = 'Manual';  EventSystem = 'Automatic';  COMSysApp = 'Manual';  Browser = 'Manual';  VaultSvc = 'Disabled';  CryptSvc = 'Automatic';  DcomLaunch = 'Automatic';  diagtrack = 'Disabled';  UxSms = 'Automatic';  Dhcp = 'Automatic';  DPS = 'Disabled';  WdiServiceHost = 'Disabled';  WdiSystemHost = 'Disabled';  defragsvc = 'Manual';  TrkWks = 'Disabled';  MSDTC = 'Manual';  Dnscache = 'Automatic';  EFS = 'Disabled';  EapHost = 'Manual';  Fax = 'Disabled';  fdPHost = 'Disabled';  FDResPub = 'Disabled';  gpsvc = 'Automatic';  hkmsvc = 'Disabled';  HomeGroupListener = 'Manual';  HomeGroupProvider = 'Manual';  hidserv = 'Disabled';  IKEEXT = 'Manual';  UI0Detect = 'Disabled';  SharedAccess = 'Disabled';  iphlpsvc = 'Disabled';  PolicyAgent = 'Manual';  KtmRm = 'Manual';  lltdsvc = 'Disabled';  Mcx2Svc = 'Disabled';  MSiSCSI = 'Disabled';  swprv = 'Manual';  MMCSS = 'Automatic';  NetTcpPortSharing = 'Disabled';  Netlogon = 'Disabled';  napagent = 'Disabled';  Netman = 'Manual';  netprofm = 'Manual';  NlaSvc = 'Automatic';  nsi = 'Automatic';  CscService = 'Disabled';  WPCSvc = 'Disabled';  PNRPsvc = 'Disabled';  p2psvc = 'Disabled';  p2pimsvc = 'Disabled';  pla = 'Manual';  PlugPlay = 'Automatic';  IPBusEnum = 'Disabled';  PNRPAutoReg = 'Disabled';  WPDBusEnum = 'Disabled';  Power = 'Automatic';  Spooler = 'Automatic';  wercplsupport = 'Disabled';  PcaSvc = 'Disabled';  ProtectedStorage = 'Manual';  QWAVE = 'Disabled';  RasAuto = 'Manual';  RasMan = 'Manual';  SessionEnv = 'Disabled';  TermService = 'Disabled';  UmRdpService = 'Disabled';  RpcSs = 'Automatic';  RpcLocator = 'Disabled';  RemoteRegistry = 'Disabled';  RemoteAccess = 'Disabled';  RpcEptMapper = 'Automatic';  seclogon = 'Manual';  SstpSvc = 'Manual';  SamSs = 'Automatic';  wscsvc = 'Automatic';  LanmanServer = 'Automatic';  ShellHWDetection = 'Automatic';  SCardSvr = 'Disabled';  SCPolicySvc = 'Disabled';  SNMPTRAP = 'Disabled';  sppsvc = 'Automatic';  sppuinotify = 'Manual';  SSDPSRV = 'Manual';  StorSvc = 'Disabled';  SysMain = 'Automatic';  SENS = 'Automatic';  TabletInputService = 'Disabled';  Schedule = 'Automatic';  lmhosts = 'Automatic';  TapiSrv = 'Manual';  Themes = 'Disabled';  THREADORDER = 'Manual';  TBS = 'Disabled';  upnphost = 'Manual';  ProfSvc = 'Automatic';  vds = 'Manual';  VSS = 'Manual';  WebClient = 'Disabled';  AudioSrv = 'Automatic';  AudioEndpointBuilder = 'Automatic';  SDRSVC = 'Manual';  WbioSrvc = 'Disabled';  idsvc = 'Disabled';  WcsPlugInService = 'Disabled';  wcncsvc = 'Disabled';  WinDefend = 'Disabled';  wudfsvc = 'Manual';  WerSvc = 'Disabled';  Wecsvc = 'Manual';  EventLog = 'Automatic';  MpsSvc = 'Automatic';  FontCache = 'Automatic';  StiSvc = 'Manual';  msiserver = 'Manual';  Winmgmt = 'Automatic';  ehRecvr = 'Disabled';  ehSched = 'Disabled';  WMPNetworkSvc = 'Disabled';  TrustedInstaller = 'Manual';  WinRM = 'Disabled';  WSearch = 'Disabled';  W32Time = 'Manual';  wuauserv = 'Manual';  WinHttpAutoProxySvc = 'Disabled';  dot3svc = 'Manual';  Wlansvc = 'Manual';  wmiApSrv = 'Manual';  LanmanWorkstation = 'Automatic';  WwanSvc = 'Disabled'
	}
	Foreach ($service_setting in $service_list.GetEnumerator()){
		if($service_setting.Value -eq "Disabled"){
			Stop-Service -Name $service_setting.Key
		}
		Set-Service $service_setting.Key -StartupType $service_setting.Value
	}
}
Start-Job -Name DisableServices -scriptblock $disable_services



# Install Package Manager I.e Chocolatey---------------------------------------------------------------------------------------------------------------------------------------------------------------
$install_programs = {
	Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


# Install Software needed------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	param($temp_folder)
	$current_user = (Get-WmiObject -Class Win32_Process -Filter 'Name="explorer.exe"').GetOwner().User
	$shell_application = new-object -c shell.application
	$programs_to_pin = @{'C:\Windows\System32' = "taskmgr.exe"}


	choco install firefoxesr -y
	$programs_to_pin.Add('C:\Program Files\Mozilla Firefox', "firefox.exe")

	choco install 7zip.install -y

	choco install mobaxterm -y
	$programs_to_pin.Add('C:\Program Files (x86)\Mobatek\MobaXterm', "MobaXterm.exe")

	choco install sublimetext3 -y
	$programs_to_pin.Add('C:\Program Files\Sublime Text 3', "sublime_text.exe")

	choco install dnspy -y
	$programs_to_pin.Add('C:\ProgramData\chocolatey\bin','dnSpy.exe')

	choco install windbg -y
	$programs_to_pin.Add('C:\Program Files (x86)\Windows Kits\10\Debuggers\x64\','windbg.exe')
	$programs_to_pin.Add('C:\Program Files (x86)\Windows Kits\10\Debuggers\x86\','windbgx86.exe')

	choco install x64dbg.portable -y
	$programs_to_pin.Add('C:\ProgramData\chocolatey\bin','x96dbg.exe')

	choco install fiddler -y
	$programs_to_pin.Add("C:\Users\$current_user\AppData\Local\Programs\Fiddler\",'Fiddler.exe')

	choco install visualstudio-installer -y

	choco install hxd -y

	if((Get-WmiObject -computer Localhost win32_computersystem).Manufacturer -eq "QEMU" ){
		$spice_source='https://www.spice-space.org/download/windows/spice-guest-tools/spice-guest-tools-latest.exe'
			$spice_dest = $temp_folder + "\spice.exe"
			$client = new-object System.Net.WebClient
			$client.DownloadFile($spice_source, $spice_dest)
			& $spice_dest /Install-Agent
	}
	Foreach($new_program in $programs_to_pin.GetEnumerator()){
		$program_name = $shell_application.namespace($new_program.Key).parsename($new_program.Value)
		$program_name.invokeverb('taskbarpin')
	}

}
Start-Job -ArgumentList $temp_folder -Name InstallPrograms -scriptblock $install_programs

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Write-Host "`n Disabling Windows Services" -ForegroundColor Green
Write-Host "`n Disabling Windows Features" -ForegroundColor Green
Write-Host "`n Removing specific Updates" -ForegroundColor Green

Write-Host "`n Waiting for All tasked to be finished."
Get-Job -Name RemoveUpdates | Receive-Job
Get-Job -Name DisableServices | Receive-Job
Get-Job -Name DisableFeatures | Receive-Job
Get-Job -Name InstallPrograms | Receive-Job
#Get-Job | Receive-Job

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

remove-item $temp_folder -Force -Recurse # Remove temp Folder
